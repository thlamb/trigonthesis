MAKEFLAGS  := -j 1

INS         = src/trigonthesis.ins
PACKAGE_SRC = $(wildcard src/*.dtx)
PACKAGE_STY = $(notdir $(PACKAGE_SRC:%.dtx=%.sty))

DOC_SRC     = doc/trigonthesis-doc.dtx
DOC_PDF     = doc/trigonthesis-doc.pdf

DESTDIR     ?= $(shell kpsewhich -var-value=TEXMFHOME)
INSTALL_DIR  = $(DESTDIR)/tex/latex/trigonthesis
DOC_DIR      = $(DESTDIR)/doc/latex/trigonthesis
LATEXMK_CONF = $(shell pwd)/latexmkrc
CACHE_DIR   := $(shell pwd)/.latex-cache

COMPILE_TEX := latexmk -xelatex -cd
CLEAN_TEX := latexmk -c -cd
CLEAN_PDF := latexmk -C -cd

export TEXINPUTS:=$(shell pwd):$(shell pwd)//:${TEXINPUTS}

.PHONY: all sty doc demo layouts screenshots tests clean install uninstall ctan clean-cache clean-sty Clean pkg-version

all: sty

sty: $(PACKAGE_STY)

doc: $(DOC_PDF)

clean: clean-cache clean-sty

Clean: clean clean-pdf clean-img

clean-cache:
	@rm -rf "$(CACHE_DIR)"
	$(CLEAN_TEX) $(PACKAGE_SRC) $(DEMO_SRC) $(DEMO_ALT_SRC) $(DOC_SRC) $(LAYOUTS_SRC) $(TESTS_SRC)

clean-pdf:
	$(CLEAN_PDF) $(PACKAGE_SRC) $(DEMO_SRC) $(DEMO_ALT_SRC) $(DOC_SRC) $(LAYOUTS_SRC) $(TESTS_SRC)

clean-sty:
	@rm -f $(PACKAGE_STY)

clean-img:
	@rm -f $(dir $(LAYOUTS_SRC))/*.jpg

pkg-version:
	@sed -i 's@v\([0-9]\{1,\}\.\)\{1,\}[0-9]\{1,\}@$(shell git describe --abbrev=0 --tags)@g' $(PACKAGE_SRC)
	@sed -i 's@\[20[0-9][0-9]/[0-9]*/[0-9]*@\[$(shell date "+%Y/%m/%d")@g' $(PACKAGE_SRC)

$(CACHE_DIR):
	@mkdir -p $(CACHE_DIR)

$(PACKAGE_STY): $(PACKAGE_SRC) $(INS) | clean-cache $(CACHE_DIR)
	@cd $(dir $(INS)) && latex -output-directory=$(CACHE_DIR) $(notdir $(INS))
	@cp $(addprefix $(CACHE_DIR)/,$(PACKAGE_STY)) .

$(DOC_PDF): $(DOC_SRC) $(PACKAGE_STY) $(LAYOUTS_IMG)
	$(CLEAN_TEX) $(DOC_SRC)
	$(COMPILE_TEX) $(DOC_SRC)
