# NOTICE

## Licenses

This project files are all released under the [Latex Project Public
License](LPPL).

### Cleanthesis

Trigonthesis is inspired by the excellent [cleanthesis] package, which is
available under the [LLPL] as well, with the following copyright:

> Copyright 2020 R. Langner


## Images, illustrations

### Trigon logo

The Trigon logo is released under the [CC-BY-SA-4.0].

### Other illustrations

- **Man in Red Jacket Standing on the Stairs** (called '_library.jpg_' in this
  repo): Photo by [Taryn Elliott from Pexels][man-red-jacket].

[MIT]: licenses/MIT.md
[GPL-3.0]: licenses/GPL-3.0.md
[Apache-2.0]: licenses/Apache-2.0.md
[W3C-20150513]: licenses/W3C-20150513.md
[CC-BY-SA-4.0]: https://creativecommons.org/licenses/by-sa/4.0/
[CC-BY-SA-3.0]: https://creativecommons.org/licenses/by-sa/3.0/
[CC0-1.0]: https://creativecommons.org/publicdomain/zero/1.0/deed.en
[LPPL]: https://www.latex-project.org/lppl/
[overleaf]: https://overleaf.com
[metro-repo]: https://github.com/matze/mtheme
[1]: https://creativecommons.org/faq/#can-i-apply-a-creative-commons-license-to-software
[2]: https://github.com/matze/mtheme/issues/387
[man-red-jacket]: https://www.pexels.com/photo/man-in-red-jacket-standing-on-the-stairs-4390730/
[cleanthesis]: https://github.com/derric/cleanthesis

