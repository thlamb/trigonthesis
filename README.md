# Trigon thesis

_A modern, elegant and versatile Latex Style for well suited for thesis and
large reports._

## References & Acknowledgment

- This package is heavily inspired by the excellent [CleanThesis] style, from
  witch it inherited a few base options.

## License

Copyright 2023 by Thomas Lambert <dev@tlambert.be>.

**Author and maintainer**: Thomas Lambert.

This theme is licensed under the [Latex Project Public License](LICENSE), which
is essentially a _free software_ license. Do not hesitate to copy and modify the
code to fit your needs.

[CleanThesis]: https://github.com/derric/cleanthesis
