% \iffalse meta-comment -------------------------------------------------------
% Copyright (C) 2023 Thomas Lambert <dev@tlambert.be>
% https://gitlab.com/thlamb/trigonthesis
%
% License LPPL 1.3c
% This file may be distributed and/or modified under the conditions of the
% LaTeX Project Public License, either version 1.3c of this license or any
% later version. The latest version of this license can be found at:
% http://www.latex-project.org/lppl.txt
% ------------------------------------------------------------------------- \fi
% \iffalse
%<*package>
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{trigonthesis}[2023/02/23 v0.0.0 Trigon Thesis]
%</package>
% \fi
% \CheckSum{0}
% \StopEventually{}
% \iffalse
%<*package>
% ------------------------------------------------------------------------- \fi
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{Package options}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% All options are processed using the |xkeyval| package.
%    \begin{macrocode}
\RequirePackage{xkeyval}      % For options
%    \end{macrocode}
%
% \begin{macro}{hangsection}
% Sets whether to use a hanging section label (placed in page margin) or not.
%    \begin{macrocode}
\define@boolkey[tt]{trigthesis}{hangsection}[true]{}
\setkeys[tt]{trigthesis}{hangsection=true}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{hangsubsection}
% Sets whether to use a hanging sub-section label (placed in page margin) or
% not.
%    \begin{macrocode}
\define@boolkey[tt]{trigthesis}{hangsubsection}[true]{}
\setkeys[tt]{trigthesis}{hangsubsection=true}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{sansserif}
% Sets whether to use a sans serif font for the whole text or not.
%    \begin{macrocode}
\define@boolkey[tt]{trigthesis}{sansserif}[false]{}
\setkeys[tt]{trigthesis}{sansserif=false}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{figuresep}
% This option can be used to define a different label separator for cations of
% figures.
%    \begin{macrocode}
\define@choicekey*[tt]{trigthesis}{figuresep}
{none,colon,period,space,quad,newline,endash}[endash]
{\def\trigthesis@figuresep{#1}}
\setkeys[tt]{trigthesis}{figuresep=endash}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{colorize}
% This option can be used to define a color mode, i.e., what elements or parts
% of the document should be colored. This allows you to reduce costs, if you
% need to print the document. The following values are allowed
%    \begin{macrocode}
\define@choicekey*[tt]{trigthesis}{colorize}[\val\colorizenr]
{full,reduced,bw}[full]{\def\trigthesis@colorize{\colorizenr}}
\setkeys[tt]{trigthesis}{colorize=full}
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{colortype}
% This pick between RGB (best for screens), HTML (best for web) and CMYK (best
% for printing) color type.
%    \begin{macrocode}
\define@choicekey*[tt]{trigthesis}{colortype}
{RGB, HTML, cmyk}[cmyk]{\def\trigthesis@colortype{#1}}
\setkeys[tt]{trigthesis}{colortype=cmyk}
%    \end{macrocode}
% \end{macro}
%
% Process options and raise errors if needed.
%    \begin{macrocode}
\DeclareOptionX*{
	\PackageWarning{trigonthesis}{Unknown option ‘\CurrentOption’}%
}
\ProcessOptionsX[tt]<trigthesis>
%    \end{macrocode}
%
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{New definitions}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% We introduce new definitions to simplify the customization of title pages and
% other major document elements (pdf metadata, urls, etc).
%    \begin{macrocode}
\def\@theurl{}
\newcommand{\theurl}[1]{
  \def\@theurl{#1}
}
\def\@theauthor{}
\newcommand{\theauthor}[1]{
  \def\@theauthor{#1}
}
\def\@thetitle{}
\newcommand{\thetitle}[1]{
  \def\@thetitle{#1}
}
\def\@thesubtitle{}
\newcommand{\thesubtitle}[1]{
  \def\@thesubtitle{#1}
}
\def\@thesubject{}
\newcommand{\thesubject}[1]{
  \def\@thesubject{#1}
}
\def\@thekeywords{}
\newcommand{\thekeywords}[1]{
  \def\@thekeywords{#1}
}
\def\@thedate{}
\newcommand{\thedate}[1]{
  \def\@thedate{#1}
}
\def\@theversion{}
\newcommand{\theversion}[1]{
  \def\@theversion{#1}
}
\def\@thesupervisor{}
\newcommand{\thesupervisor}[1]{
  \def\@thesupervisor{#1}
}
\def\@theuniversity{}
\newcommand{\theuniversity}[1]{
  \def\@theuniversity{#1}
}
\def\@thedepartment{}
\newcommand{\thedepartment}[1]{
  \def\@thedepartment{#1}
}
\def\@theresearchgroup{}
\newcommand{\theresearchgroup}[1]{
  \def\@theresearchgroup{#1}
}
\def\@thecity{}
\newcommand{\thecity}[1]{
  \def\@thecity{#1}
}
\def\@thezipcode{}
\newcommand{\thezipcode}[1]{
  \def\@thezipcode{#1}
}
\def\@thecountry{}
\newcommand{\thecountry}[1]{
  \def\@thecountry{#1}
}


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{General packages dependencies}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%    \begin{macrocode}
\RequirePackage{microtype}    % Better layout/spacing
\RequirePackage[onehalfspacing]{setspaceenhanced} % Line spacing
\RequirePackage{graphicx}	  	% Graphics import features
\RequirePackage{booktabs}			% Better looking tables
\RequirePackage{enumitem}			% List modifications
\RequirePackage{hyperref} 		% Links and hyperref
\RequirePackage{calc}         % Set length, counters, etc
\RequirePackage[automark]{scrlayer-scrpage} % Control header and footer for KOMA
\RequirePackage[dvipsnames]{xcolor}       % Colors
% Modify figure and table captions
\RequirePackage[
	font={small},
	labelfont={bf,sf,color=ttcolorfloatlabel},
	labelsep=\trigthesis@figuresep,
  singlelinecheck=true
]{caption}
%    \end{macrocode}
%
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsubsection{Package additional configuration}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We first make sure the hyperref package displays the links in a pleasant way.
%
%    \begin{macrocode}
\hypersetup{
  breaklinks=true,
  bookmarksnumbered=true,
  bookmarksopen=true,
  colorlinks=true,
  linkcolor = tBlueGreen,
  citecolor = tGreen,
  filecolor = tBlue,
  urlcolor = tBlue,
  plainpages=false,
  pdfborder={0 0 0},
}
%    \end{macrocode}
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{Colors}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Before defining the colors, we set the color model to use according to package
% options.
%    \begin{macrocode}
\selectcolormodel{\trigthesis@colortype}
%    \end{macrocode}
%
% \subsection{Base palette definition}
%
%    \begin{macrocode}
\definecolor{tBlueGreen}{RGB/HTML/cmyk}{0,112,127/00707F/1,0.35,0.45,0.1}
\definecolor{tBlueGreenLight}{RGB/HTML/cmyk}{95,164,176/5FA4B0/0.65,0.2,0.3,0}
\definecolor{tBeigeLight}{RGB/HTML/cmyk}{232,226,222/E8E2DE/0.1,0.1,0.12,0}
\definecolor{tBeigePale}{RGB/HTML/cmyk}{230,230,225/E6E6E1/0.05,0.05,0.05,0}
\definecolor{tBeige}{RGB/HTML/cmyk}{198,192,180/C6C0B4/0.27,0.22,0.3,0}
\definecolor{tYellow}{RGB/HTML/cmyk}{255,208,0/FFD000/0,0.18,1,0}
\definecolor{tYellowOrange}{RGB/HTML/cmyk}{248,170,0/F8AA00/0,0.38,1,0}
\definecolor{tOrange}{RGB/HTML/cmyk}{240,127,60/F07F3C/0,0.6,0.8,0}
\definecolor{tRed}{RGB/HTML/cmyk}{230,45,49/E62D31/0,0.92,0.8,0}
\definecolor{tGreenPale}{RGB/HTML/cmyk}{185,205,118/B9CD76/0.35,0.05,0.65,0}
\definecolor{tGreenLight}{RGB/HTML/cmyk}{125,185,40/7DB928/0.58,0,1,0}
\definecolor{tGreen}{RGB/HTML/cmyk}{40,155,56/289B38/0.8,0.1,1,0}
\definecolor{tGreenDark}{RGB/HTML/cmyk}{0,132,59/00843B/1,0.1,1,0.1}
\definecolor{tBlueLight}{RGB/HTML/cmyk}{31,186,219/1FBADB/0.7,0,0.12,0}
\definecolor{tBlue}{RGB/HTML/cmyk}{0,92,169/005CA9/1,0.6,0,0}
\definecolor{tBlueViolet}{RGB/HTML/cmyk}{91,87,162/5B57A2/0.75,0.7,0,0}
\definecolor{tLavander}{RGB/HTML/cmyk}{141,166,214/8DA6D6/0.5,0.3,0,0}
\definecolor{tPurple}{RGB/HTML/cmyk}{168,88,158/A8589E/0.4,0.75,0,0}
\definecolor{tViolet}{RGB/HTML/cmyk}{91,37,125/5B257D/0.8,1,0.06,0.02}
\definecolor{tGrey}{RGB/HTML/cmyk}{140,139,130/8C8B82/0.2,0.15,0.25,0.45}
\definecolor{tGreyLight}{RGB/HTML/cmyk}{181,180,169/B5B4A9/0.3,0.22,0.3,0.08}
\definecolor{tWhite}{RGB/HTML/cmyk}{255,255,255/FFFFFF/0,0,0,0}
%    \end{macrocode}
%
% \subsubsection{Main theme colors}
%
% All the main trigon elements are set using the base colors defined previously.
%    \begin{macrocode}
\colorlet{tPrim}{tBlueGreen}
\colorlet{tSec}{tBlueGreen}
\colorlet{tAccent}{tOrange}
\colorlet{tTxt}{black}
\colorlet{tGrey}{tBeige}
\colorlet{tTheme}{tBlueGreen}
%    \end{macrocode}
%
% \subsubsection{Set the colors for each element}
% Set all colors as if we were using colorize = full, then overwrite if reduced
% or bw
%    \begin{macrocode}
% -- footer colors
\colorlet{ttcolorfooterpage}{tTxt}
\colorlet{ttcolorfooterline}{tTheme}
\colorlet{ttcolorfootermark}{tTxt}
\colorlet{ttcolorfootertitle}{tTheme}
% -- sectioning colors
\colorlet{ttcolortitle}{tTheme}
\colorlet{ttcolorpartnum}{tTheme}
\colorlet{ttcolorpartline}{tTxt}
\colorlet{ttcolorparttext}{tTxt}
\colorlet{ttcolorchapternum}{tTheme}
\colorlet{ttcolorchapterline}{tTheme}
\colorlet{ttcolorsection}{tTheme}
\colorlet{ttcolorsubsection}{tTheme}
\colorlet{ttcolorparagraph}{tTxt}
% --> text colors
\colorlet{ttcolorfloatlabel}{tSec}
\colorlet{ttcolorlistlabel}{tSec}

\ifcase\trigthesis@colorize
  % case = 0 (colorize == full)
  % do nothing
  \or
  % case = 1 (colorize == reduced)
  % --> footer colors
  \colorlet{ttcolorfooterline}{tGrey}
  \colorlet{ttcolorfootertitle}{tGrey}
  % --> sectioning colors
  \colorlet{ttcolorsubsection}{tTxt}
  \colorlet{ttcolorpartnum}{tTxt}
  % --> text colors
  \colorlet{ttcolorlistlabel}{tTxt}
  \or
  % case = 2 (colorize == bw)
  % --> footer colors
  \colorlet{ttcolorfooterline}{tTxt}
  \colorlet{ttcolorfootertitle}{tTxt}
  % --> sectioning colors
  \colorlet{ttcolortitle}{tTxt}
  \colorlet{ttcolorpartnum}{tTxt}
  \colorlet{ttcolorchapternum}{tTxt}
  \colorlet{ttcolorchapterline}{tTxt}
  \colorlet{ttcolorsection}{tTxt}
  \colorlet{ttcolorsubsection}{tTxt}
  % --> text colors
  \colorlet{ttcolorfloatlabel}{tTxt}
  \colorlet{ttcolorlistlabel}{tTxt}
\else
\fi
%    \end{macrocode}
%
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{Fonts}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \subsubsection{Fonts definition}
% The default font for text and math is XCharter. The headings, and some other
% layout elements (float labels, etc) use the sans serif font Source Sans
% Pro. A minor adaptation was made so the default bold font is actually semibold
% for Source Sans Pro.
%    \begin{macrocode}
% From https://tex.stackexchange.com/a/536379
\RequirePackage{fontspec}
\RequirePackage{xcharter-otf}
\setsansfont{sourcesanspro}[
  Extension=.otf,
  UprightFont=*-Regular,
  ItalicFont=*-RegularIt,
  BoldFont=*-Semibold,
  BoldItalicFont=*-SemiboldIt,
  FontFace={xl}{n}{*-ExtraLight},
  FontFace={xl}{it}{*-ExtraLightIt},
  FontFace={l}{n}{*-Light},
  FontFace={l}{it}{*-LightIt},
  FontFace={mb}{n}{*-Semibold},
  FontFace={mb}{it}{*-SemiboldIt},
  FontFace={k}{n}{*-Black},
  FontFace={k}{it}{*-BlackIt},
]
%    \end{macrocode}
%
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{Layout}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \subsubsection{Page construction}
% General KOMA-script options to customize the page layout. Header and footer
% are deactivated here in order to be precisely set later on.
%
%    \begin{macrocode}
\KOMAoption{headinclude}{false}     % include header in body?
\KOMAoption{footinclude}{false}     % include footer in body?
\KOMAoption{footlines}{2.1}         % number of foot lines
\KOMAoption{mpinclude}{false}       % include marginpar in body?
\KOMAoption{DIV}{12}                % number of page divs (divider)
%    \end{macrocode}
%
% \subsubsection{Text formatting}
% Basic options for the text format.
%
%    \begin{macrocode}
\clubpenalty = 10000				% prevent single lines at the beginning of a paragraph (Schusterjungen)
\widowpenalty = 10000				% prevent single lines at the end of a paragraph (Hurenkinder)
\displaywidowpenalty = 10000		%
%    \end{macrocode}
%
% \subsubsection{Lists definitions}
% List and enumerate environment improvements.
%
%    \begin{macrocode}
%    \end{macrocode}
%
%
% \subsubsection{Header and footer}
% Definition of the header and footer of the document.
%
%    \begin{macrocode}
%    \end{macrocode}
%
%
%
% \subsection{Sectionning}
%
%    \begin{macrocode}
%    \end{macrocode}
% ------------------------------------------------------------------------------
% \iffalse
%</package>
% \fi
% \Finale
\endinput

